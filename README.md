# BitbucketServerClient README #

A simple client for bitbucket server. Initially built to get pull requests for all projects by a date.

## PRE-SETUP ##
* Download and Install Core Dependencies 
    * JDK 8
    * Groovy
    * Gradle

## SETUP ##

* Clone the repo
* Project expects to find a file at ~/.stash/credentials.config this is configurable via the application.properties file and BitbucketConfiguration.groovy
* Make sure config file has stash.username and stash.password fields. 


If using intellij:
* Import into Intellij using Gradle and Auto-import (Verified works on Intellij 13-15)
* Check project settings are set to Jdk8
* Check that project has refreshed the Gradle dependencies (Gradle Tools Window -> can be found with Ctrl + Shift + a) Little blue refresh icon near top left.

### Resources ###

* [Spring Boot Getting Started Guide](https://spring.io/guides/gs/spring-boot/)
* [Groovy Template Engine + Spring Boot](https://spring.io/blog/2014/05/28/using-the-innovative-groovy-template-engine-in-spring-boot)