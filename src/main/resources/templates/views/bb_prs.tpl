div() {
    div() {yield "Project count:  Repo count: $prsByProjectAndRepo.size PullRequest Count: ${prsByProjectAndRepo.sum{p->p.pullRequests.size}}"}
    select(id:"repoJump") {
    prsByProjectAndRepo.each { prGroup ->
            option(
                value:"$prGroup.project.name-$prGroup.repo.name",
                "($prGroup.project.name)$prGroup.repo.name ($prGroup.pullRequests.size)"
            )
        }
    }
    script() {
        yieldUnescaped """
        var select = document.getElementById('repoJump');
        select.onchange = function(){
            console.warn(this.value);
            var url = location.href;               //Save down the URL without hash.
                location.href = "#"+this.value;    //Go to the target element.
                history.replaceState(null,null,url);
        }
        """
    }
}
div(class:"PullRequestWrapper") {
    prsByProjectAndRepo.each { prGroup ->
        def projectPullRequests = [host:host, project:prGroup.project, repo:prGroup.repo, pullRequests:prGroup.pullRequests]
        layout projectPullRequests, 'views/bb_pr.tpl'
    }
}