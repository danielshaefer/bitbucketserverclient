import java.time.Instant;
import java.time.ZoneOffset;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import org.ma.model.UTC;

div(style:"background-color:lightblue;padding:10px 5px",id:"$project.name-$repo.name") {
    yield "Project: $project.name($project.key), Repo: $repo.name, Record count: " + pullRequests.size()
}
pullRequests.each { pr ->
    div(style:"border:1px solid black;margin-left: 20px;") {
        div {
            a(href:"$host/$pr.link.url", "Pull Request Id: $pr.id")
        }
        div {
            yield "Author: $pr.author.user.displayName"
        }
        div {
            pr.reviewers.addAll(pr.participants);
            def approvers = pr.reviewers.collect {
                if (it.approved)
                    return "$it.user.displayName"
            }
            yield "Approvers: " + approvers.findAll().join(",")
        }
        div {
            yield "Updated Date: " + UTC.fromMillisToString(pr.updatedDate.toLong()) + " UTC"
        }
    }
}