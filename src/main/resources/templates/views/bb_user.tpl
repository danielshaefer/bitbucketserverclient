layout 'layouts/main.tpl',
        pageTitle: 'Spring Boot - Groovy templates example with layout',
        mainBody: contents {

            div("Username: $username")
            div("Display Name: $display_name")
        }