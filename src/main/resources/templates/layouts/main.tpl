//http://docs.groovy-lang.org/latest/html/documentation/template-engines.html#_the_markuptemplateengine
yieldUnescaped '<!DOCTYPE html>'
html {
    head {
        title(pageTitle)
        link(rel: 'stylesheet', href: 'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css')
    }
    body {
        mainBody()
    }
}