package org.ma.controller

import groovy.json.JsonSlurper
import org.ma.model.BitbucketServerClient
import org.ma.model.RequestCache
import org.ma.model.UTC
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.beans.factory.annotation.Qualifier
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.Banner
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import org.springframework.web.servlet.ModelAndView
import org.springframework.web.servlet.mvc.method.annotation.RequestMappingHandlerMapping

/**
 * Groovy Annotated Rest Controller*
 */
@RestController
@RequestMapping("/")
class BitbucketApiController {

    @Value('${cache.path}')
    private String CACHE_PATH
    private RequestCache requestCache = new RequestCache();
    private RequestMappingHandlerMapping requestMapping
    @Autowired @Qualifier("bitbucketconfig")
    ConfigObject config

    @Autowired
    public BitbucketApiController(RequestMappingHandlerMapping requestMapping) {
        this.requestMapping = requestMapping
    }
    
    @RequestMapping("/")
    String index() {
        def endpoints = requestMapping.getHandlerMethods().collect{it.key.patternsCondition.patterns}.join(",<br>")
        "Mapped Endpoints: $endpoints"
        
    }

    /**
     * Example endpoint returning using Groovy Template files*
     * @return
     */
    @RequestMapping("/home") //this mapping is currently necessary to handle trailing slashes throwing errors.
    def home() {
        println "Home endpoint on Groovy Controller"
        new ModelAndView(
                    "views/home",
                    [bootVersion: Banner.package.implementationVersion,
                     groovyVersion: GroovySystem.version])
        
    }

    /**
     * Example of simple http calling in Groovy*
     */
    @RequestMapping("/bitbucket/user/{username}")
    def bitbucket(@PathVariable String username) {
        def client2 = new BitbucketServerClient("https://bitbucket.org")
        def userData = client2.getBitbucketUser(username)
        return new ModelAndView(
                "views/bb_user",
                userData)
    }

    /**
     * Get pull requests for a given project and repo*
     */
    @RequestMapping("/bitbucket/pullRequests/{project}/{repo}")
    def bitbucket(@PathVariable String project, @PathVariable String repo, @RequestParam(required = false) String raw) {
        def client1 = new BitbucketServerClient(config.stash.host).withCredentials(CACHE_PATH);
        def data = client1.getAllPullRequestsForProject(project, repo);
        if (raw)
            return data.raw
        return new ModelAndView(
                "views/bb_prs",
                [pullRequests:  data.raw.values,
                 projectKey: data.project,
                 repo: data.repo
                ])
    }

    /**
     * Get repos for the all projects or the specified projects.
     * @param projectName
     * @return
     */
    @RequestMapping("/bitbucket/projects/repos")
    def bitbucketAllProjectsAllRepos(@RequestParam(required=false) String projectName) {
        def client = new BitbucketServerClient(config.stash.host).withCredentials(CACHE_PATH);
        def projectObjs = client.getProjects(projectName).projects.collect {[projectKey: it.key, projectName: it.name]}
        def allReposByProject = projectObjs.collect { proj ->
            def repos = client.getReposFor(proj.projectKey)
            proj.repoCount = repos.size()
            [project:proj, repos:repos.collect{it.name}]
        }
        return allReposByProject
    }

    /**
     * Get all projects
     * @return
     */
    @RequestMapping("/bitbucket/projects")
    def bitbucketAllProjects() {
        def client = new BitbucketServerClient(config.stash.host).withCredentials(CACHE_PATH);
        def projects = client.getProjects();
        return projects
    }

    /**
     * Get all pull requests for all projects and repos or for the specified project.
     * @param raw
     * @param projectName
     * @param excludeEmpty
     * @return
     */
    @RequestMapping("/bitbucket/projects/pullRequests")
    def bitbucketAllProjectsPullRequests(@RequestParam(required = false) String raw,
                                         @RequestParam(required=false) String projectName,
                                         @RequestParam(required=false) String excludeEmpty,
                                         @RequestParam(required=false) Integer randomSelectionCount) {

        def cacheFileName = requestCache.buildHashForFileName(raw, projectName, excludeEmpty)
        def cacheFile = new File(CACHE_PATH, cacheFileName)
        if (cacheFile.exists()) {
            println "FROM CACHE: $cacheFileName"
            def data =  new JsonSlurper().parse(cacheFile)
            if (randomSelectionCount)
                data = selectRandomPullRequests(data, randomSelectionCount)
            checkAllPRsForAuthorApproverMatch(data.prsByProjectAndRepo)
            return new ModelAndView(
                    "layouts/pullRequests",
                    data)
        }
        def client = new BitbucketServerClient(config.stash.host).withCredentials(CACHE_PATH);
        def projectObjs = client.getProjects(projectName).projects.collect {[key: it.key, name: it.name]}
        def allPullRequests = [];
        projectObjs.each { proj ->
            def repos = client.getReposFor(proj.key).collect {[name: it.slug, key: it.project.key]}
            repos.each { r ->
                def prData = client.getAllPullRequestsForProject(proj.key, r.name, UTC.NewYears2016());
                if (!(prData.pullRequests.size() == 0 && excludeEmpty))
                    allPullRequests += [
                            pullRequests: prData.pullRequests,
                            raw: prData.raw,
                            repo: r,
                            project: proj
                    ]
            }
        }
        def templateObj = [host: client.host,
                           projects: projectObjs,
                           prsByProjectAndRepo: allPullRequests]
        checkAllPRsForAuthorApproverMatch(allPullRequests)

        if (randomSelectionCount)
            templateObj = randomSelectionCount(templateObj, randomSelectionCount)
        requestCache.cacheToFile(client.pathToCredentials, templateObj, raw, projectName, excludeEmpty)

        if (raw)
            return allPullRequests.collect{it.raw}
        return new ModelAndView(
                "layouts/pullRequests",
                templateObj)
    }

    public def checkAllPRsForAuthorApproverMatch(allPullRequests) {
        println "Checking all Pull Requests for an approver that is not the author"
        allPullRequests.each { prs ->
            prs.pullRequests.each { pr ->
                def result = checkForAuthorApproverMatch(pr)
                if (!result) {
                    println "FAILED RESULT"
                    println pr
                    throw new RuntimeException("Same approver as author and only 1 approver.")
                }
            }
        }
    }

    public def checkForAuthorApproverMatch(pr) {
        def author = pr.author.user.displayName

        pr.reviewers.addAll(pr.participants);
        def approvers = pr.reviewers.collect {
            if (it.approved)
                return "$it.user.displayName"
        }
        if (approvers.size() > 1 || (approvers.size() == 1 && approvers[0] != author)) {
            return true
        } else {
            return false
        }
    }

    private selectRandomPullRequests(data, int randomSelectionCount) {
        def collectedPullRequests = data.prsByProjectAndRepo.collect { it.pullRequests }.flatten()
        Collections.shuffle(collectedPullRequests)
        def selected = collectedPullRequests[0..(randomSelectionCount - 1)]
        def selectedPullRequests = [
                pullRequests: selected,
                repo        : [name: "Various", key: "Various"],
                project     : [name: "Various", key: "Various"]
        ]
        def newData = [host               : data.host,
                       prsByProjectAndRepo: [selectedPullRequests]]
        newData
    }


}
