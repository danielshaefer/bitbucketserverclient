package org.ma.controller

import org.springframework.beans.factory.annotation.Value
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration

/**
 * Created by dshaefer on 8/31/2016.
 */
@Configuration
class BitbucketConfiguration {

    def static DEFAULT_CREDENTIALS_FILE_NAME = "credentials.config"

    @Value('${cache.path}')
    private String configPath

    @Bean(name="bitbucketconfig")
    ConfigObject getBitbucketConfig() {
        new ConfigSlurper().parse(new File(configPath, DEFAULT_CREDENTIALS_FILE_NAME).toURI().toURL())
    }

}
