package org.ma.model

class UrlBuilder {
    
    def address
    def authString

    def static DEFAULT_CREDENTIALS_FILE_NAME = "credentials.config"

    public UrlBuilder(String address) {
        this.address = address;
    }

    public UrlBuilder withAuth(String pathToCredentials) {
        def config = new ConfigSlurper().parse(new File(pathToCredentials, DEFAULT_CREDENTIALS_FILE_NAME).toURI().toURL())
        this.authString = "$config.stash.username:$config.stash.password".getBytes().encodeBase64().toString()
        return this
    }

    public def toConnection() {
        def conn = address.toURL().openConnection()
        if (authString)
            conn.setRequestProperty( "Authorization", "Basic ${authString}" )
        return conn;
    }
}
