package org.ma.model

import groovy.json.JsonSlurper

import java.time.LocalDate
import java.time.LocalDateTime
import java.time.ZoneOffset

/**
 * Created by dshaefer on 1/29/2016.
 */
class BitbucketServerClient {

    String host;
    String pathToCredentials

    public BitbucketServerClient(host) {
        this.host = host;
    }

    public BitbucketServerClient withCredentials(pathToCredentials) {
        this.pathToCredentials = pathToCredentials
        return this
    }

    public def getBitbucketUser(username) {
        def addr = buildBitbucketUserRequestUrl(host, username)
        println addr
        callEndpointAndReturnJsonResponse(addr)
    }

    public def getAllPullRequestsForProject(project, repo, LocalDateTime afterDate = UTC.fromMillisToLocalDateTime(0)) {
        def lastResp = [isLastPage: false, nextPageStart: 0, isPastDateLimit: false]
        def data = [];
        while(!lastResp.isLastPage && !lastResp.isPastDateLimit) {
            lastResp = getPullRequestsForProject(project, repo, afterDate, lastResp.nextPageStart);
            data += lastResp
        }
        return [
                host: host,
                project: project,
                repo: repo,
                raw: data.collect{it.raw},
                pullRequests: data.collect{it.pullRequests}.flatten()
        ]
    }

    public def getPullRequestsForProject(project, repo, LocalDateTime afterDate, start = 0) {
        def addr = buildMergedPullRequestUrl(host, project, repo, start);
        println addr
        def url = new UrlBuilder(addr)
        if (pathToCredentials)
            url.withAuth(pathToCredentials);
        def conn = url.toConnection();
        if( conn.responseCode == 200 ) {
            def data = new JsonSlurper().parseText( conn.content.text )
            //println "Response $data"
            def afterDateMillis = afterDate.toEpochSecond(ZoneOffset.UTC) * 1000
            return [isLastPage: data.isLastPage,
                    isPastDateLimit: data.values.updatedDate.min() < afterDateMillis,
                    nextPageStart: data.nextPageStart,
                    raw: data,
                    pullRequests: data.values.findAll{it.updatedDate >= afterDateMillis}];
        } else {
            println "Something bad happened."
            println "${conn.responseCode}: ${conn.responseMessage}"
            throw new RuntimeException(conn.responseMessage)
        }
    }

    public def getProjects(name = "") {
        def addr = "$host/rest/api/1.0/projects";
        if (name)
            addr += "?name=$name"
        def data = callEndpointHandlePaging(addr)
        def flattenedData = data.collect{it.values}.flatten()
        def projects = flattenedData.collect{[key:it.key, name:it.name, description:it.description, link:it.links.self[0].href]}
        return [projectCount: projects.size(), projects:projects]
    }

    public def getReposFor(String project) {
        def baseAddr = "$host/rest/api/1.0/projects/$project/repos"
        def data = callEndpointHandlePaging(baseAddr)
        data.collect{it.values}.flatten()
    }

    private def callEndpointHandlePaging(baseAddr) {
        def addr = "";
        def lastResp = [isLastPage: false, nextPageStart: 0]
        def data = [];
        while(!lastResp.isLastPage) {
            if (!baseAddr.contains("?"))
                addr = baseAddr + "?start=$lastResp.nextPageStart"
            else
                addr = baseAddr + "&start=$lastResp.nextPageStart"
            lastResp = callEndpointAndReturnJsonResponse(addr)
            data += lastResp
        }
        return data;
    }

    private def callEndpointAndReturnJsonResponse(addr) {
        println addr
        def url = new UrlBuilder(addr)
        if (pathToCredentials)
            url.withAuth(pathToCredentials);
        def conn = url.toConnection()
        if( conn.responseCode == 200 ) {
            new JsonSlurper().parseText( conn.content.text )
        } else {
            println "Something bad happened."
            println "${conn.responseCode}: ${conn.responseMessage}"
            throw new RuntimeException(conn.responseMessage)
        }
    }

    private String buildBitbucketUserRequestUrl(host, username) {
        "$host/api/2.0/users/$username"
    }

    private String buildMergedPullRequestUrl(host, project, repo, start) {
        "$host/rest/api/1.0/projects/$project/repos/$repo/pull-requests?state=merged&start=$start";
    }


}