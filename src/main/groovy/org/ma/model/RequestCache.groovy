package org.ma.model

import groovy.json.JsonBuilder

import java.security.MessageDigest

/**
 * Created by dshaefer on 2/2/2016.
 */
class RequestCache {

    def cacheToFile(path, data, String... params) {
        def hash = buildHashForFileName(params)
        def cacheFile = new File(path, hash)
        cacheFile.delete();
        cacheFile << new JsonBuilder(data).toString();
    }

    def buildHashForFileName(String... params) {
        generateMD5_A(buildRequestCacheString(params))
    }

    def buildRequestCacheString(String... params) {
        params.join()
    }

    def generateMD5_A(String s){
        MessageDigest.getInstance("MD5").digest(s.bytes).encodeHex().toString()
    }

}
