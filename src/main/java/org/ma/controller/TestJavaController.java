package org.ma.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Java annotated Rest Controller*
 */
@RestController
@RequestMapping("/admin")
public class TestJavaController {

    @RequestMapping("/")
    public String index() {
        return "Greetings from Spring Boot Root Controller!";
    }

    @RequestMapping(value={"/test", "/test/"}) //this solves trailing slash problems. try the /admin without the trailing slash and you will get an error.
    String test() {
        return "do something interesting";
    }
}